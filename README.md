# Bandit algs for warfarin dosing

CS234 course project. See details in `writeup.pdf`.

`tester.py` runs the experiments. `plot.py` creates plots.
