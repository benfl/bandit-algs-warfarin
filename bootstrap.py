import numpy as np
import pandas as pd
from sklearn import tree
from sklearn.utils import resample

class TreeBootstrap(object):
    """
    An agent to learn a linear UCB algorithm for a contextual bandit problem.
    """
    def __init__(self, arms):
        self.count = 0
        self.track_record = []
        self.arms = arms
        self.seen = {arm: ([], []) for arm in arms}
        self.seen_both = {arm: False for arm in arms}

    def step(self, row, truth):
        oldp = -np.inf
        abort = False

        for key, e in self.seen_both.items():
            if not e:
                abort = True
                action = key

        if not abort:
            for key, data in self.seen.items():
                clf = tree.DecisionTreeClassifier(min_weight_fraction_leaf=0.05)#, class_weight='balanced')
                X, y = resample(*data)
                while len(set(y)) == 1:
                    X, y = resample(*data)
                clf.fit(X, y)

                p = clf.predict_proba([row])[0][1]
                if p >= oldp:
                    action = key
                    oldp = p

        success = action == truth

        self.seen[action] = (self.seen[action][0] + [row], self.seen[action][1] + [success])
        if len(set(self.seen[action][1])) > 1:
            self.seen_both[action] = True
        self.count += 1
        self.track_record += [success]

    def regret(self):
        return sum(np.logical_not(self.track_record))

    def cum_acc(self):
        return np.cumsum(self.track_record) / np.arange(1, self.count + 1)

    def cum_reg(self):
        return np.cumsum(np.logical_not(self.track_record))

    def acc(self):
        return sum(self.track_record) / self.count
