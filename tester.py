import numpy as np
import pandas as pd
from linucb import LinUCB
from thompson import Thompson
from bootstrap import TreeBootstrap

def binner(x):
    if (x > 49):
        return 'high'
    elif (x >= 21):
        return 'medium'
    elif x >= 0:
        return 'low'
    else:
        return np.nan

def numer(s):
    try:
        return int(s[0])
    except Exception:
        return np.nan

def clinical(fr):
    return (4.0376 - 0.2546 * fr['Age (decades)'] + 0.0118 * fr['Height (cm)']
            + 0.0134 * fr['Weight (kg)'] - 0.6752 * (fr['Race'] == 'Asian')
            + 0.406 * (fr['Race'] == 'Black or African American') + 0.0443 * (
                fr['Race'] == 'Unknown') + 1.2799 * fr['Enzyme inducer'] -
                       0.5695 * fr['Amiodarone (Cordarone)']) ** 2

def pharmacogenetic(fr):
    return (5.6044 - 0.2614*fr['Age (decades)'] + 0.0087 * fr['Height (cm)']
            + 0.0128 * fr['Weight (kg)'] - 0.8677 * (
                fr['VKORC1 -1639 consensus'] == 'A/G') - 1.6974 * (
                    fr['VKORC1 -1639 consensus'] == 'A/A') - 0.4854 * (
                        fr['VKORC1 -1639 consensus'] == 'Unknown') -
            0.5211 * (fr['CYP2C9 consensus'] == '*1/*2') -
            0.9357 * (fr['CYP2C9 consensus'] == '*1/*3') -
            1.0616 * (fr['CYP2C9 consensus'] == '*2/*2') -
            1.9206 * (fr['CYP2C9 consensus'] == '*2/*3') -
            2.3312 * (fr['CYP2C9 consensus'] == '*3/*3') -
            0.2188 * (fr['CYP2C9 consensus'] == 'Unknown') -
            0.1092 * (fr['Race'] == 'Asian') -
            0.2760 * (fr['Race'] == 'Black or African American') -
            0.1032 * (fr['Race'] == 'Unknown') +
            1.1816 * fr['Enzyme inducer'] -
            0.5503 * fr['Amiodarone (Cordarone)']
            ) ** 2

def impute_vkorc(fr):
    if not (fr['Race'] == 'Black or African American' or
            fr['Race'] == 'Unknown'):
        if fr['VKORC1 2255 consensus'] == 'C/C':
            return 'G/G'
        elif fr['VKORC1 2255 consensus'] == 'T/T':
            return 'A/A'
        elif fr['VKORC1 2255 consensus'] == 'C/T':
            return 'A/G'

    if fr['VKORC1 1173 consensus'] == 'C/C':
        return 'G/G'
    elif fr['VKORC1 1173 consensus'] == 'T/T':
        return 'A/A'
    elif fr['VKORC1 1173 consensus'] == 'C/T':
        return 'A/G'

    if not (fr['Race'] == 'Black or African American' or
            fr['Race'] == 'Unknown'):
        if fr['VKORC1 1542 consensus'] == 'G/G':
            return 'G/G'
        elif fr['VKORC1 1542 consensus'] == 'C/C':
            return 'A/A'
        elif fr['VKORC1 1542 consensus'] == 'C/G':
            return 'A/G'

    return 'Unknown'

def enz(fr):
    if(fr['Carbamazepine (Tegretol)'] == 1.0 or
       fr['Phenytoin (Dilantin)'] == 1.0 or
       fr['Rifampin or Rifampicin'] == 1.0):
        return 1.0
    elif(pd.isna(fr['Carbamazepine (Tegretol)']) or
         pd.isna(fr['Phenytoin (Dilantin)']) or
         pd.isna(fr['Rifampin or Rifampicin'])):
        return 0.0 # guessing most are not enzyme inducers
    else:
        return 0.0


data = pd.read_csv('data/warfarin.csv')
data = data[pd.notnull(data['Therapeutic Dose of Warfarin'])]
data['CYP2C9 consensus'] = data['CYP2C9 consensus'].fillna('Unknown')
#d = pd.get_dummies(d, columns=['VKORC1 -1639 consensus', 'CYP2C9 consensus'],
#                   prefix_sep=' ')
data['Dose Binned'] = data['Therapeutic Dose of Warfarin'].apply(binner)
data['Age (decades)'] = data['Age'].apply(numer)
data['Enzyme inducer'] = data.apply(enz, axis=1)

data['Clinical'] = clinical(data)
data['Clinical Binned'] = data['Clinical'].apply(binner)
data['Pharm'] = pharmacogenetic(data)
data['Pharm Binned'] = data['Pharm'].apply(binner)

imputed = data.copy()
imputed['Height (cm)'].fillna(
    data.groupby('Gender')['Height (cm)'].transform('mean'), inplace=True
)
imputed['Weight (kg)'].fillna(
    data.groupby('Gender')['Weight (kg)'].transform('mean'), inplace=True
)
imputed['Amiodarone (Cordarone)'].fillna(0, inplace=True)
imputed['Age (decades)'].fillna(7, inplace=True)
imputed['VKORC1 -1639 consensus'].fillna(imputed.apply(impute_vkorc, axis=1),
                                         inplace=True)

features = ['Gender', 'Race', 'Age (decades)', 'Height (cm)', 'Weight (kg)', 'Enzyme inducer', 'VKORC1 -1639 consensus', 'CYP2C9 consensus']
data = imputed[features]
truth = imputed['Dose Binned']

dummies = pd.get_dummies(data[['Gender', 'Race', 'VKORC1 -1639 consensus',
                               'CYP2C9 consensus']], prefix_sep=' ')
data = data.drop(['Gender', 'Race', 'VKORC1 -1639 consensus',
                  'CYP2C9 consensus'], axis=1)
data = pd.concat([data, dummies], axis=1)
data = data.drop(['Gender male', 'Race White', 'VKORC1 -1639 consensus G/G', 'CYP2C9 consensus *1/*1'], axis=1)
data['bias'] = 1

d = len(data.columns)

#params = {lab: (np.eye(d), np.zeros(d)) for lab in ['low', 'medium', 'high']}
#p = dict.fromkeys(['low', 'medium', 'high'])
#data['Height (cm)'] = data['Height (cm)'] / 100
#data['Weight (kg)'] = data['Weight (kg)'] / 100
#data['Age (decades)'] = data['Age (decades)'] / 10
#alpha = 0.5
#res = []

#data = data.iloc[np.random.permutation(5528)]
viewed = {bla: [] for bla in ['lin', 'thomp', 'boots']}
for c in range(10):
    #5528
    data = data.iloc[np.random.permutation(5528)]
    #for le in ['lin', 'thomp', 'boots']:
    for le in ['thomp']:
        print(c, le)
        if le == 'lin':
            learner = LinUCB(d, ['low', 'medium', 'high'])
        elif le == 'thomp':
            learner = Thompson(d, ['low', 'medium', 'high'])
        else:
            learner = TreeBootstrap(['low', 'medium', 'high'])

        print('hi?')
        for i, x in data.iterrows():
            learner.step(x, truth[i])
            print(i,  learner.cum_acc()[-1])

        print('hi?')
        viewed[le] += [learner.cum_reg()]
        print(learner.cum_acc()[-1])


#for le in ['lin', 'thomp', 'boots']:
#    np.savetxt(le + '.csv', np.array(viewed[le]), delimiter=',', fmt='%d')
