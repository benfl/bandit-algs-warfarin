import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

lin = np.loadtxt(open('lin.csv', 'rb'), delimiter=',')
thomp = np.loadtxt(open('thomp.csv', 'rb'), delimiter=',')
boots = np.loadtxt(open('boots.csv', 'rb'), delimiter=',')

lin_mean = lin.mean(axis=0)
lin_std = lin.std(axis=0)

thomp_mean = thomp.mean(axis=0)
thomp_std = thomp.std(axis=0)

boots_mean = boots.mean(axis=0)
boots_std = boots.std(axis=0)

_, ax = plt.subplots()
ax.set_aspect(1)
plt.plot(np.arange(1, 5529), lin_mean, linestyle='--')
plt.fill_between(np.arange(1, 5529), lin_mean - lin_std, lin_mean + lin_std, alpha=0.5)
plt.plot(np.arange(1, 5529), thomp_mean, linestyle=':')
plt.fill_between(np.arange(1, 5529), thomp_mean - thomp_std, thomp_mean + thomp_std, alpha=0.5)
plt.plot(np.arange(1, 5529), boots.mean(axis=0), linestyle='-.')
plt.fill_between(np.arange(1, 5529), boots_mean - boots_std, boots_mean + boots_std, alpha=0.5)
plt.xlabel('iteration')
plt.ylabel('regret')
plt.legend(['LinUCB', 'Thompson sampling', 'TreeBootstrap'])
plt.savefig('regret-plot.pdf', bbox_inches='tight')
plt.clf()

lin_acc_mean = 1 - lin_mean / np.arange(1, 5529)
thomp_acc_mean = 1 - thomp_mean / np.arange(1, 5529)
boots_acc_mean = 1 - boots_mean / np.arange(1, 5529)

lin_acc_std = lin_std / np.arange(1, 5529)
thomp_acc_std = thomp_std / np.arange(1, 5529)
boots_acc_std = boots_std / np.arange(1, 5529)

_, ax = plt.subplots()
ax.set_aspect(8000)

plt.plot([1, 5529], [0.691, 0.691], color='maroon')
plt.plot([1, 5529], [0.643, 0.643], color='darkolivegreen')
plt.plot([1, 5529], [0.612, 0.612], color='gold')
plt.plot(np.arange(1, 5529), lin_acc_mean, linestyle='--')
plt.fill_between(np.arange(1, 5529), lin_acc_mean - lin_acc_std, lin_acc_mean + lin_acc_std, alpha=0.5)
plt.plot(np.arange(1, 5529), thomp_acc_mean, linestyle=':')
plt.fill_between(np.arange(1, 5529), thomp_acc_mean - thomp_acc_std, thomp_acc_mean + thomp_acc_std, alpha=0.5)
plt.plot(np.arange(1, 5529), boots_acc_mean, linestyle='-.')
plt.fill_between(np.arange(1, 5529), boots_acc_mean - boots_acc_std, boots_acc_mean + boots_acc_std, alpha=0.5)
plt.xlabel('iteration')
plt.ylabel('accuracy')
plt.ylim(0.4, 0.7)
plt.legend(['Pharmacogenetic', 'Clinical', 'Fixed medium', 'LinUCB', 'Thompson sampling', 'TreeBootstrap'])
plt.savefig('acc-plot.pdf', bbox_inches='tight')
plt.clf()

_, ax = plt.subplots()
ax.set_aspect(40)
plt.plot([0, 4], [0.691, 0.691], color='maroon')
plt.plot([0, 4], [0.643, 0.643], color='darkolivegreen')
plt.plot([0, 4], [0.612, 0.612], color='gold')
plt.legend(['Pharmacogenetic', 'Clinical', 'Fixed medium'])
plt.boxplot([1 - lin[:, -1] / 5528, 1 - thomp[:, -1] / 5528, 1 - boots[:, -1] / 5528])
plt.ylabel('accuracy')
plt.xticks([1, 2, 3], ['LinUCB', 'Thompson\n sampling', 'TreeBootstrap'])
plt.savefig('boxplot.pdf', bbox_inches='tight')
