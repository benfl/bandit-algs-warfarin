import numpy as np
import pandas as pd

class Thompson(object):
    """
    An agent to learn a linear UCB algorithm for a contextual bandit problem.
    """
    def __init__(self, n_features, arms, v_sq=0.05):
        self.params = {arm: (np.eye(n_features),
                             np.zeros(n_features)) for arm in arms}
        self.count = 0
        self.track_record = []
        self.v_sq = v_sq

    def step(self, row, truth):
        oldp = -np.inf

        for key, par in self.params.items():
            B, f = par
            Binv = np.linalg.inv(B)
            mu_hat = Binv @ f

            sample = np.random.multivariate_normal(mu_hat, self.v_sq * Binv)
            p = sample @ row
            if p >= oldp:
                action = key
                oldp = p
                currB = B
                currf = f

        success = action == truth
        self.params[action] = (currB + np.outer(row, row), currf + success * row)
        self.count += 1
        self.track_record += [success]

    def regret(self):
        return sum(np.logical_not(self.track_record))

    def cum_acc(self):
        return np.cumsum(self.track_record) / np.arange(1, self.count + 1)

    def cum_reg(self):
        return np.cumsum(np.logical_not(self.track_record))

    def acc(self):
        return sum(self.track_record) / self.count
