import numpy as np
import pandas as pd

def binner(x):
    if (x > 49):
        return 'high'
    elif (x >= 21):
        return 'medium'
    elif x >= 0:
        return 'low'
    else:
        return np.nan

def numer(s):
    try:
        return int(s[0])
    except Exception:
        return np.nan

def clinical(fr):
    return (4.0376 - 0.2546 * fr['Age Binned'] + 0.0118 * fr['Height (cm)']
            + 0.0134 * fr['Weight (kg)'] - 0.6752 * (fr['Race'] == 'Asian')
            + 0.406 * (fr['Race'] == 'Black or African American') + 0.0443 * (
                fr['Race'] == 'Unknown') + 1.2799 * fr['Enzyme inducer'] -
                       0.5695 * fr['Amiodarone (Cordarone)']) ** 2

def pharmacogenetic(fr):
    return (5.6044 - 0.2614*fr['Age Binned'] + 0.0087 * fr['Height (cm)']
            + 0.0128 * fr['Weight (kg)'] - 0.8677 * (
                fr['VKORC1 -1639 consensus'] == 'A/G') - 1.6974 * (
                    fr['VKORC1 -1639 consensus'] == 'A/A') - 0.4854 * (
                        fr['VKORC1 -1639 consensus'] == 'Unknown') -
            0.5211 * (fr['CYP2C9 consensus'] == '*1/*2') -
            0.9357 * (fr['CYP2C9 consensus'] == '*1/*3') -
            1.0616 * (fr['CYP2C9 consensus'] == '*2/*2') -
            1.9206 * (fr['CYP2C9 consensus'] == '*2/*3') -
            2.3312 * (fr['CYP2C9 consensus'] == '*3/*3') -
            0.2188 * (fr['CYP2C9 consensus'] == 'Unknown') -
            0.1092 * (fr['Race'] == 'Asian') -
            0.2760 * (fr['Race'] == 'Black or African American') -
            0.1032 * (fr['Race'] == 'Unknown') +
            1.1816 * fr['Enzyme inducer'] -
            0.5503 * fr['Amiodarone (Cordarone)']
            ) ** 2

def impute_vkorc(fr):
    if not (fr['Race'] == 'Black or African American' or
            fr['Race'] == 'Unknown'):
        if fr['VKORC1 2255 consensus'] == 'C/C':
            return 'G/G'
        elif fr['VKORC1 2255 consensus'] == 'T/T':
            return 'A/A'
        elif fr['VKORC1 2255 consensus'] == 'C/T':
            return 'A/G'

    if fr['VKORC1 1173 consensus'] == 'C/C':
        return 'G/G'
    elif fr['VKORC1 1173 consensus'] == 'T/T':
        return 'A/A'
    elif fr['VKORC1 1173 consensus'] == 'C/T':
        return 'A/G'

    if not (fr['Race'] == 'Black or African American' or
            fr['Race'] == 'Unknown'):
        if fr['VKORC1 1542 consensus'] == 'G/G':
            return 'G/G'
        elif fr['VKORC1 1542 consensus'] == 'C/C':
            return 'A/A'
        elif fr['VKORC1 1542 consensus'] == 'C/G':
            return 'A/G'

    return 'Unknown'

def enz(fr):
    if(fr['Carbamazepine (Tegretol)'] == 1.0 or
       fr['Phenytoin (Dilantin)'] == 1.0 or
       fr['Rifampin or Rifampicin'] == 1.0):
        return True
    elif(pd.isna(fr['Carbamazepine (Tegretol)']) or
         pd.isna(fr['Phenytoin (Dilantin)']) or
         pd.isna(fr['Rifampin or Rifampicin'])):
        return np.nan
    else:
        return False


d = pd.read_csv('data/warfarin.csv')
d = d[pd.notnull(d['Therapeutic Dose of Warfarin'])]
d['CYP2C9 consensus'] = d['CYP2C9 consensus'].fillna('Unknown')
#d = pd.get_dummies(d, columns=['VKORC1 -1639 consensus', 'CYP2C9 consensus'],
#                   prefix_sep=' ')
d['Dose Binned'] = d['Therapeutic Dose of Warfarin'].apply(binner)
d['Age Binned'] = d['Age'].apply(numer)
d['Enzyme inducer'] = d.apply(enz, axis=1)

d['Clinical'] = clinical(d)
d['Clinical Binned'] = d['Clinical'].apply(binner)
d['Pharm'] = pharmacogenetic(d)
d['Pharm Binned'] = d['Pharm'].apply(binner)

imputed = d.copy()
imputed['Height (cm)'].fillna(
    d.groupby('Gender')['Height (cm)'].transform('mean'), inplace=True
)
imputed['Weight (kg)'].fillna(
    d.groupby('Gender')['Weight (kg)'].transform('mean'), inplace=True
)
imputed['Amiodarone (Cordarone)'].fillna(0, inplace=True)
imputed['Age Binned'].fillna(7, inplace=True)
imputed['VKORC1 -1639 consensus'] = imputed['VKORC1 -1639 consensus'].fillna(
    imputed.apply(impute_vkorc, axis=1)
)
imputed['Enzyme inducer'].fillna(False, inplace=True)

imputed['Clinical'] = clinical(imputed)
imputed['Clinical Binned'] = imputed['Clinical'].apply(binner)
imputed['Pharm'] = pharmacogenetic(imputed)
imputed['Pharm Binned'] = imputed['Pharm'].apply(binner)
