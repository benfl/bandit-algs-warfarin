import numpy as np
import pandas as pd

class LinUCB(object):
    """
    An agent to learn a linear UCB algorithm for a contextual bandit problem.
    """
    def __init__(self, n_features, arms, alpha=0.5):
        self.params = {arm: (np.eye(n_features),
                             np.zeros(n_features)) for arm in arms}
        self.count = 0
        self.track_record = []
        self.alpha = alpha

    def step(self, row, truth):
        oldp = -np.inf

        for key, par in self.params.items():
            A, b = par
            Ainv = np.linalg.inv(A)
            theta = Ainv @ b
            p = theta @ row + self.alpha * np.sqrt(row @ Ainv @ row)
            if p >= oldp:
                action = key
                oldp = p
                currA = A
                currb = b

        success = action == truth
        self.params[action] = (currA + np.outer(row, row), currb + success * row)
        self.count += 1
        self.track_record += [success]

    def regret(self):
        return sum(np.logical_not(self.track_record))

    def cum_acc(self):
        return np.cumsum(self.track_record) / np.arange(1, self.count + 1)

    def cum_reg(self):
        return np.cumsum(np.logical_not(self.track_record))

    def acc(self):
        return sum(self.track_record) / self.count
